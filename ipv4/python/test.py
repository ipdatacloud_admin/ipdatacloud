#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time


import ip


class IpInfo():
    def __init__(self,results):
        #洲
        self.continent = results[0]
        #国家
        self.country = results[1]
        #国家英文简写
        self.countryCode = results[8]
        #省份
        self.province = results[2]
        #城市
        self.city = results[3]
        #区县
        self.district = results[4]
        #区域代码
        self.areaCode = results[6]
        #运营商
        self.isp = results[5]
        #经度
        self.longitude = results[9]
        #纬度
        self.latitude = results[10]
        #海拔
        self.elevation = results[11]
        #气象站
        self.weatherStation = results[14]
        #邮编
        self.zipCode = results[12]
        #城市代码
        self.cityCode = results[13]
        self.asn = results[15]
        self.timeZone = results[18]

ip_search = ip.IPV4Find(r"ipdatacloud_city.dat")
ip_search.get("49.81.179.93")
result = ip_search.get("49.81.179.93")
results = result.split("|")
ipinfo = IpInfo(results)
for field_name, field_value in ipinfo.__dict__.items():
    print(f"{field_name}: {field_value}")
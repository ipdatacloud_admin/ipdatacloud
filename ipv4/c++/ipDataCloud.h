#ifndef __IP_H4_
#define __IP_H4_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tag_ip {
    uint32_t prefStart[256];
    uint32_t prefEnd[256];
    uint8_t *buffer;
} geo_ip;

geo_ip *ip_instance(const char *fileName);

int32_t ip_loadDat(geo_ip *p, const char *fileName);

char *ip_query(geo_ip *p, const char *ip);

long ip_binary_search(geo_ip *p, long low, long high, long k);

uint32_t ip_ip2long(geo_ip *p, const char *addr, uint32_t *prefix);

uint32_t ip_read_int32(geo_ip *p, int pos);


char *get_addr(geo_ip *p, uint32_t j);
;
#ifdef __cplusplus
}
#endif
#endif

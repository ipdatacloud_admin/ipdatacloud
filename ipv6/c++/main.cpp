#include "ipDataCloud_v6.h"
#include <memory>
#include <iostream>


int main() {
    std::unique_ptr<geo_ipv6> finder_v6(ipv6_instance("file_path_v6"));
    const char* ip = "ip_address";
    char* result = ipv6_query(finder_v6.get(), ip);
    std::cout << result << std::endl;
    free(result);
    result = NULL;
}
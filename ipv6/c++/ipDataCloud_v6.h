#ifndef __IP_H6_
#define __IP_H6_

#include <stdint.h>
#include <stdlib.h>
#include "uint128_t.h"

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct tag_ipv6
	{
		uint32_t prefStart[65600];
		uint32_t prefEnd[65600];
		uint8_t* data;
		long numbers;
	}geo_ipv6;

	geo_ipv6* ipv6_instance(const char* file_path);
	int32_t ipv6_loadDat(geo_ipv6* p,const char* file_path);
	long getPref(const char* ip);
	char* ipv6_query(geo_ipv6* p, const char *ip);
	long ipv6_binary_search(geo_ipv6* p, long low, long high, uint128_t k);
	uint32_t ipv6_read_int32(geo_ipv6* p, int pos);


#ifdef __cplusplus
}
#endif
#endif

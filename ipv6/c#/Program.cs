﻿namespace newTest;

public class Test
{
    public static void Main()
    {
        // 1.初始化查询对象
        IPSearch search = IPSearch.Instance;

        // 2.执行查询操作
        string ip = "2404:be40:ffff:ffff:ffff:ffff:ffff:ffff";
        string res = search.Find(ip);
        Console.WriteLine(res);
    }
}